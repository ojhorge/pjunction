# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create(username: "jorgem2r", email: "jorge.miranda.tpsid@gmail.com", password: "12345678", password_confirmation: "12345678")
User.create(username: "jorgem3r", email: "j.miranda@my.westminster.ac.uk", password: "12345678", password_confirmation: "12345678")
User.create(username: "jorgem4r", email: "jorge.m89@gmail.com", password: "12345678", password_confirmation: "12345678")
User.create(username: "jorgem5r", email: "jorge.miguel@gmail.com", password: "12345678", password_confirmation: "12345678")
User.create(username: "jorgem6r", email: "jorge.miranda@gmail.com", password: "12345678", password_confirmation: "12345678")

p "Users created successfully"